import { Injectable } from '@nestjs/common';
import * as os from 'os';

@Injectable()
export class AppService {
  getHello(): string {
    let str:string = 'Hello, Beautiful World!<br/>';
    str += new Date() + '<br/>' + os.hostname();
    return str
  }
}
